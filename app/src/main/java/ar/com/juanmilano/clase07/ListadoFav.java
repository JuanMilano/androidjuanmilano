package ar.com.juanmilano.clase07;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListadoFav extends Activity {
    ListView tablaFav;
    String urlname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_fav);

        MiHelperBD helper = new MiHelperBD(this, "MiBase", null, 1);

        SQLiteDatabase db = helper.getReadableDatabase();

        Cursor c = db.rawQuery("Select * from Favoritos", null);

        List<String> arrayFav = new ArrayList<String>();
        while(c.moveToNext()){
            String idUrl = c.getString(c.getColumnIndex("id"));
            arrayFav.add(idUrl);
            urlname = c.getString(c.getColumnIndex("web"));
            arrayFav.add(urlname);
        }
            final ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayFav);
            tablaFav = (ListView)findViewById(R.id.tablaFav);
            tablaFav.setAdapter(adaptador);
            tablaFav.setOnItemClickListener(new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = new Intent();
                    String urlback = (String) tablaFav.getItemAtPosition(position);
                    returnIntent.putExtra("result",urlback);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });





    }

}
