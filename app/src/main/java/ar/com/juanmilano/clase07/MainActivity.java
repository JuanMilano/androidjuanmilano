package ar.com.juanmilano.clase07;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    WebView webView;
    EditText search;
    EditText id;
    Button cargar;
    Button guardar;
    Button buscarDb;
    public String web;
    public String idWeb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView)findViewById(R.id.webView);
        search = (EditText)findViewById(R.id.search);
        cargar = (Button)findViewById(R.id.cargar);
        guardar = (Button)findViewById(R.id.guardar);
        buscarDb = (Button)findViewById(R.id.buscarDb);
        id = (EditText) findViewById(R.id.id);


    }
        public void cargarUrl(View v){
        web = "https://"+search.getText().toString();
        webView.loadUrl(web);
}


            public void guardarFav(View v){
            MiHelperBD helper = new MiHelperBD(this, "MiBase", null, 1);
            SQLiteDatabase db = helper.getWritableDatabase();

            idWeb = id.getText().toString();
            web = search.getText().toString();

            if(db != null)
            {


                ContentValues registro = new ContentValues();
                registro.put("id", idWeb);
                registro.put("web", web);

                db.insert("Favoritos", null, registro);

                db.close();


                Toast.makeText(this, "Su página se guardo con exito!",
                        Toast.LENGTH_SHORT).show();
            }
        }

        public void buscarDB(View v){

            MiHelperBD helper = new MiHelperBD(this, "MiBase", null, 1);

            SQLiteDatabase db = helper.getReadableDatabase();

            String idBuscar = id.getText().toString();

            Cursor c = db.rawQuery("Select * from Favoritos Where id="+idBuscar, null);


            if (c.moveToFirst()) {
                do {
                   Integer id = c.getInt(0);
                   String urldb = c.getString(1);


                    search.setText(urldb);
                } while (c.moveToNext());
            }


    }

    public void listar(View V){
        Intent i = new Intent(this, ListadoFav.class);
        startActivityForResult(i, 1);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
         if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String returnUrl = data.getStringExtra("result");
                search.setText(returnUrl);
                web = "https://"+returnUrl;
                webView.loadUrl(web);
                id.setText("");

            }
        }
    }

}




