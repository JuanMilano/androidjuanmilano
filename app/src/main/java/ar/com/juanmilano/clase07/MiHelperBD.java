package ar.com.juanmilano.clase07;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MiHelperBD extends SQLiteOpenHelper {
    public MiHelperBD(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }



    public void onCreate(SQLiteDatabase bd) {

        bd.execSQL("CREATE TABLE Favoritos (id INTEGER PRIMARY KEY,web TEXT)");

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int vAnt, int vNueva) {

        // Log.i("Mensaje de la app", "Pase por este bloque");

        //db.execSQL("ALTER TABLE Contactos ADD Celular varchar(10)");
    }
}
